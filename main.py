from datetime import datetime

from tkinter import Tk

import numpy as np
from mss import mss,tools
import pyperclip


import ocr

with mss() as sct:

    monitor = sct.monitors[1]
    sct_img = np.array(sct.grab(monitor))

    screenshot_data = ocr.recognize(sct_img)

    match screenshot_data:
        case ocr.ScreenshotData(hu=val) if val is not None:
            pyperclip.copy(f'{val} HU')
        case ocr.ScreenshotData(size=val) if val is not None and len(val)==2:
            second,first = sorted(val)
            pyperclip.copy(f'{first}х{second} мм(срез {screenshot_data.slice_num})')
        case {'size': val}:
            print(screenshot_data)
            print(val)
        case _:
            print(screenshot_data)






