from dataclasses import dataclass

from datetime import datetime
import re
import pytesseract
import cv2
import numpy as np

import io
pytesseract.pytesseract.tesseract_cmd = r'C:\Users\nesterovdv\AppData\Local\Tesseract-OCR\tesseract.exe'

@dataclass
class ScreenshotData:
    slice_num: int
    hu: int = None
    eff_d: int = None
    size: list[int] = None



def recognize(photo) -> ScreenshotData:

    img_hsv = cv2.cvtColor(photo,cv2.COLOR_BGR2HSV)

    mask_green = cv2.inRange(img_hsv,(44,50,30),(69,255,255))
    mask_green = cv2.bitwise_not(mask_green)
    mask_yellow = cv2.inRange(img_hsv,(7,34,93),(17,255,255))
    mask_yellow = cv2.bitwise_not(mask_yellow)
    #choose_mask_intervals(img_hsv)
    #cv2.imshow('img',mask_yellow)
    #cv2.waitKey()
    custom_config_measurements = r'--oem 3 --psm 11 -c tessedit_char_whitelist=0123456789.-HUmAvED:  '
    custom_config_info = r'--oem 3 --psm 3'
    measurements = pytesseract.image_to_data(mask_green, config=custom_config_measurements, output_type=pytesseract.Output.DICT)
    series_info = pytesseract.image_to_data(mask_yellow, config=custom_config_info, output_type=pytesseract.Output.DICT)



    output= ScreenshotData(hu=parse_measurement_w_prefix(measurements,'av'),
                           eff_d=parse_measurement_w_prefix(measurements,'ed'),
                           size=parse_measurement_w_suffix(measurements,'mm'),
                           slice_num=get_num_after_text(series_info,"Slice"))



    return output

def get_series_time(img_dat: pytesseract.Output.DICT):
    time_string = parse_patient_info(img_dat, 3, range(20)).replace(" ","")
    try:
        time_string_clean = re.search('(\d{1,2}.{3},\d{4}/\d{2}:\d{2}:\d{2})',time_string).group(1)
        time = datetime.strptime(time_string_clean,'%d%b,%Y/%H:%M:%S')
        return time
    except AttributeError:
        return time_string

def parse_patient_info(img_dat: pytesseract.Output.DICT, line_num: int, word_num: int | list[int]):
    word_num = [word_num] if type(word_num)==int else word_num
    output_text = ''
    for l,w,text in zip(img_dat['line_num'],img_dat['word_num'],img_dat['text']):
        if l==line_num and w in word_num:
            output_text+= ' '+text
    if output_text=='':
        raise IndexError(f'No text with line number {line_num} and word number {word_num}.')
    return output_text


def parse_measurement_w_prefix(img_dat: pytesseract.Output.DICT, prefix: str):
    for t in img_dat['text']:
        if t.lower().startswith(prefix.lower()):
            return re.findall('-?\d+', t)[0]
    return None

def parse_measurement_w_suffix(img_dat: pytesseract.Output.DICT, prefix: str) -> list[int]:
    output=[]
    for t in img_dat['text']:
        if t.lower().endswith(prefix.lower()):
            output.append(int(re.findall('-?\d+', t)[0]))
    return output

def get_num_after_text(img_dat: pytesseract.Output.DICT, text: str) -> int:
    text_idx = img_dat['text'].index(text)
    t_page,t_block,t_par,t_line,t_word = [img_dat[k][text_idx] for k in ['page_num','block_num','par_num','line_num','word_num']]

    text_word_num = img_dat['word_num'][text_idx]
    for t,page,block,par,line,word in zip(*[img_dat[k] for k in ['text','page_num','block_num','par_num','line_num','word_num']]):
        if page==t_page and block==t_block and par == t_par and line==t_line and word==t_word+1:
            return int(re.match('(\d*)',t).group(1))


def print_by_line(img_dat: pytesseract.Output.DICT):
    prev_i = 0
    for i,text in zip((img_dat['line_num']),(img_dat['text'])):
        print(text,end=' ')
        if i!=prev_i:
            print('\n')
        prev_i=i





def choose_mask_intervals(img):
    cv2.namedWindow('HSV')

    def nothing(x):
        pass
    cv2.createTrackbar("h_from", "HSV", 0, 360,nothing)
    cv2.createTrackbar("h_to", "HSV", 0, 360, nothing)
    cv2.createTrackbar("s_from", "HSV", 0, 360, nothing)
    cv2.createTrackbar("s_to", "HSV", 0, 360, nothing)
    cv2.createTrackbar("v_from", "HSV", 0, 360, nothing)
    cv2.createTrackbar("v_to", "HSV", 0, 360, nothing)
    cv2.createTrackbar("blured", "HSV", 1, 10, nothing)


    while (1):
        h_from = int(cv2.getTrackbarPos('h_from',"HSV"))
        h_to = int(cv2.getTrackbarPos('h_to', "HSV"))
        s_from = int(cv2.getTrackbarPos('s_from', "HSV"))
        s_to = int(cv2.getTrackbarPos('s_to', "HSV"))
        v_from = int(cv2.getTrackbarPos('v_from', "HSV"))
        v_to = int(cv2.getTrackbarPos('v_to', "HSV"))
        blur = int(cv2.getTrackbarPos('blured', "HSV"))

        mask_green = cv2.inRange(cv2.blur(img, (1+2*blur,1+2*blur)), (h_from, s_from, v_from), (h_to, s_to, v_to))

        cv2.imshow('img', mask_green )

        # waitfor the user to press escape and break the while loop
        k = cv2.waitKey(1) & 0xFF
        if k == 27:
            break

    # destroys all window
    cv2.destroyAllWindows()
